///<summary>
/// Абстрактная фабрика (AbstractFactory) представляет интерфейс для создания взаимосвязанных или зависимых (dependent or related) объектов.
/// При этом Абстрактная фабрика не задает конкретные классы создаваемых объектов 
///</summary>
///<demo>
/// Демонстрационный пример показывает применение Абстрактной Фабрики для изменения базового поведения.
/// Мы рассматриваем программу "Шахматы" в которой пользователи хотят видеть разные визуализации фигур - кроме базовой еще и "Классическая", "Современная"
/// У нас очень странные шахматы - фигур всего две, пешки и короли, но это СЕМЕЙСТВА фигур а для производства СЕМЕЙСТВ подходят специализированные ФАБРИКИ
/// Ниже определяем перечисление ChessKind для типов фигур

///</demo>
using System.Text;
using SimpleChess;

namespace GOFPatterns.Creational
{
    public enum ChessKind
    {
        Basic,
        Classic,
        Modern
    }


    public class ClassicPawn : IChessShape
    {
        public void Draw()
        {
            Console.WriteLine("Classic Pawn");
        }
    }

    public class ClassicKing : IChessShape
    {
        public void Draw()
        {
            Console.WriteLine("Classic King");
        }
    }

    public class ModernPawn : IChessShape
    {
        public void Draw()
        {
            Console.WriteLine("Modern Pawn");
        }
    }

    public class ModernKing : IChessShape
    {
        public void Draw()
        {
            Console.WriteLine("Modern King");
        }
    }


    /// Собственно абстрактная фабрика от которой будут наследоваться конкретные
    public abstract class ChessShapeFactory
    {
        public abstract IChessShape Create(ChessShape shape);
    }

    /// Фабрика базовых фигур
    public class BasicChessShapeFactory : ChessShapeFactory
    {
        public override IChessShape Create(ChessShape shape)
        {
            switch (shape)
            {
                case ChessShape.Pawn:
                    return new Pawn();
                case ChessShape.King:
                    return new King();
                default:
                    throw new ArgumentOutOfRangeException(nameof(shape), shape, null);
            }
        }
    }


    /// Фабрика "классических" фигур
    public class ClassicChessShapeFactory : ChessShapeFactory
    {
        public override IChessShape Create(ChessShape shape)
        {
            switch (shape)
            {
                case ChessShape.Pawn:
                    return new ClassicPawn();
                case ChessShape.King:
                    return new ClassicKing();
                default:
                    throw new ArgumentOutOfRangeException(nameof(shape), shape, null);
            }
        }
    }

    /// Фабрика "современных" фигур
    public class ModernChessShapeFactory : ChessShapeFactory
    {
        public override IChessShape Create(ChessShape shape)
        {
            switch (shape)
            {
                case ChessShape.Pawn:
                    return new ModernPawn();
                case ChessShape.King:
                    return new ModernKing();
                default:
                    throw new ArgumentOutOfRangeException(nameof(shape), shape, null);
            }
        }
    }

    public class AbstractFactoryDemo
    {
        public static ChessShapeFactory GetFactory(ChessKind kind)
        {
            switch (kind)
            {
                case ChessKind.Classic:
                    return new ClassicChessShapeFactory();
                case ChessKind.Modern:
                    return new ModernChessShapeFactory();
                default:
                    return new BasicChessShapeFactory();
            }
        }

        public static void Run()
        {
            var basic = GetFactory(ChessKind.Basic);
            var King = basic.Create(ChessShape.King);
            King.Draw();
        }
    }

}
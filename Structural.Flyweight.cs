///<summary>
///  Легковес (Flyweight) обеспечивает эклномное хранениев памяти крупных объектов совместно используемых разными экземплярами класса
///  В данном примере (очень простом, реально экономия не имеет смысла) оптимизируем класс Книга у которого есть атрибуты наименование и Автор
///  Первый вариант просто хранит в каждом экземпляре атрибуты (строки)
///  Второй вариант применяет "магию" хранит список уникальных строк которые встречаются в именах книг и авторов. А в самих экземплярах класса 
///  храниятся массивы индексов уникальных слов в списке
///</summary>

using System.Collections.Generic;

namespace GOFPatterns.Structural
{
    /// Пример рбычного класса - хранит атрибуты в экземпляре класса
    public class Book
    {
        public string Title { get; }
        public string Author { get; }
        public Book(string bookTitle, string authorName)
        {
            Title = bookTitle;
            Author = authorName;
        }
    }

    public class FlyweightBook
    {
        /// То что делает легковесом - храним массив уникальных слов встречающихся в именах
        static List<string> names = new List<string>();
        /// Храним не название книги а массив индексов слов составляющих название
        private int[] idxTitles;
        /// Храним не автора книги а массив индексов слов составляющих имя автора
        private int[] idxAuthors;

        public static int ListSize()
        {
            return names.Count;
        }

        public FlyweightBook(string bookTitle, string authorName)
        {
            int getOrAdd(string s)
            {
                int idx = names.IndexOf(s);
                if (idx != -1) return idx;
                else
                {
                    names.Add(s);
                    return names.Count - 1;
                }
            }

            idxTitles = bookTitle.Split(' ').Select(getOrAdd).ToArray();
            idxAuthors = authorName.Split(' ').Select(getOrAdd).ToArray();
        }

        public string Title => string.Join(" ", idxTitles.Select(i => names[i]));
        public string Author => string.Join(" ", idxAuthors.Select(i => names[i]));
    }

    public class FlyweightDemo
    {
        public static void Run()
        {
            List<FlyweightBook> books = new List<FlyweightBook>()
            {
                new FlyweightBook("Война и мир", "Лев Толстой"),
                new FlyweightBook("Первая русская книга для чтения", "Лев Толстой"),
                new FlyweightBook("Bторая русская книга для чтения", "Лев Толстой"),
                new FlyweightBook("Третья русская книга для чтения", "Лев Толстой"),
                new FlyweightBook("Четвертая русская книга для чтения", "Лев Толстой")
            };

            foreach (FlyweightBook book in books)
            {
                Console.WriteLine($"{book.Title} - {book.Author}");
            }

            Console.WriteLine($"Легковес хранит только {FlyweightBook.ListSize()} слов");

        }
    }
}
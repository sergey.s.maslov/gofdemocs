///<summary>
/// Мост (Bridge) позволяет упростить иерархию классов за счет разделения ее на две иерархии связанные "мостом"
/// При этом наследование заменяется делегированием
///</summary>
///<demo>
/// Демонстрационный пример показывает применение Моста для решения задачи в которой нужно обеспечить рисование
/// различных геометрических фигур используя векторную либо растровую графику. Всесто возможной иерархии в
/// которой появлялись бы "Растровый круг" и "Векторный квадрат" мы проектируем иерархию фигур и иерархию "рендеров"
///</demo>

using static System.Console;

namespace GOFPatterns.Structural
{
    public interface IRenderer
    {
        void RenderCircle(float radius);
    }

    public class VectorRenderer : IRenderer
    {
        public void RenderCircle(float radius)
        {
            WriteLine($"Рисуем круг радиуса {radius}");
        }
    }

    public class RasterRenderer : IRenderer
    {
        public void RenderCircle(float radius)
        {
            WriteLine($"Рисуем точки круга радиусом {radius}");
        }
    }

    public abstract class Shape
    {
        protected IRenderer renderer;

        // Мост будет между рисуемым объектом и компонентом который рисует
        public Shape(IRenderer renderer)
        {
            this.renderer = renderer;
        }

        public abstract void Draw();
        public abstract void Resize(float factor);
    }

    public class Circle : Shape
    {
        private float radius;

        public Circle(IRenderer renderer, float radius) : base(renderer)
        {
            this.radius = radius;
        }

        public override void Draw()
        {
            renderer.RenderCircle(radius);
        }

        public override void Resize(float factor)
        {
            radius *= factor;
        }
    }

    public class BridgeDemo
    {
        public static void Run()
        {
            var raster = new RasterRenderer();
            var vector = new VectorRenderer();
            var circle = new Circle(vector, 5);
            circle.Draw();
            circle.Resize(2);
            circle.Draw();

        }
    }
}
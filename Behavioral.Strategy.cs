///<summary>
///  Инкапсулирует семейство алгоритмов и обеспечивает возможность их взаимозамены
///  Изолирует алгоритм от использующих его клиентов
///</summary>
///<>
///<>

namespace GOFPatterns.Behavioral
{
    /// Маршрут как список точек. Для примера, просто пустышка
    public class Route
    {

    }

    /// Маршруты могут быть разных типов
    public enum RouteKind
    {
        Walk,
        Bus,
        Car
    }

    /// Базовый класс всех стратегий (можно поменять на интерфейс - будет аналогичное решение)
    /// Определяет общий интерфейс алгоритма
    public abstract class RouteStrategy
    {
        public abstract Route buildRoute(string A, string B);
    }

    /// 
    public class WalkRouteStrategy : RouteStrategy
    {
        public override Route buildRoute(string A, string B)
        {
            Console.WriteLine("Walk");
            return new Route();
        }
    }

    public class BusRouteStrategy : RouteStrategy
    {
        public override Route buildRoute(string A, string B)
        {
            Console.WriteLine("Bus");
            return new Route();
        }
    }

    public class CarRouteStrategy : RouteStrategy
    {
        public override Route buildRoute(string A, string B)
        {
            Console.WriteLine("Car");
            return new Route();
        }
    }

    /// Контекст - контейнер для стретегии, в данном случае - навигатор
    /// Динамический вариант реализации. Стратегии можно менять после создания навигатора
    public class Navigator
    {
        private RouteStrategy _strategy;

        public Navigator()
        {
            // По умолчанию пеший маршрут 
            _strategy = new WalkRouteStrategy();
        }

        public void SetRouteKind(RouteKind routeKind)
        {
            switch (routeKind)
            {
                case RouteKind.Walk:
                    _strategy = new WalkRouteStrategy();
                    break;
                case RouteKind.Bus:
                    _strategy = new BusRouteStrategy();
                    break;
                case RouteKind.Car:
                    _strategy = new CarRouteStrategy();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(routeKind), routeKind, null);
            }
        }

        public Route BuildRoute(string A, string B)
        {
            Console.WriteLine($"строим маршрут из {A} в {B}");
            return _strategy.buildRoute(A, B);
        }
    }


    public class StaticNavigator<RS>
    where RS : RouteStrategy, new()
    {
        private RouteStrategy _strategy = new RS();

        public Route BuildRoute(string A, string B)
        {
            Console.WriteLine($"строим маршрут из {A} в {B}");
            return _strategy.buildRoute(A, B);
        }
    }


    public class StrategyDemo
    {
        public static void Run()
        {
            var navigator = new Navigator();
            /// Стратегия поиска пешего маршрута - используется по умолчанию
            navigator.BuildRoute("Пункт А", "Пункт Б");
            ///  Меняем стратегию
            navigator.SetRouteKind(RouteKind.Car);
            /// Строим маршрут на машине
            navigator.BuildRoute("Пункт Б", "Пункт В");

            ///Используем статический навигатор - стратегия задается на все время существования
            var snavigator = new StaticNavigator<BusRouteStrategy>();
            snavigator.BuildRoute("Пункт В", "Пункт Г");
        }
    }

}
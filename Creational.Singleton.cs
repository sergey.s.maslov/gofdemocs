///<summary>
/// Класс, гарантирующий создание единственного экземпляра. Часто используется для организации глобальной точки доступа к какому-то уникаьному ресурсу
/// Должен обеспечивать корректну работу в много поточной среде
/// Желательно чтобы класс был "ленивым" (не инстанцировался до первого обращения)
///</summary>
///<problem>
///  Вносит глобальный контекст, что само по себе обычно считается плохой практикой
///<problem>
///<alternames>
///  Ambient Context (тоже анти-паттерн, использовать с осторожностью)
///</alternates>
///<dotnet>
///  Классические Singleton
///  System.Net.NetworkingPerfCounters.Instance
///  System.Threading.TimerQueue.Instance
///  ...
///  Настраиваемые (Ambient Context)
///  Thread.CurrentThread
///  HTttpContext.Current
///</dotnet>
using System.Text;

namespace GOFPatterns.Creational
{
    ///<summary>
    ///  Для версий .NET 4/0+
    ///  Реализация Одиночки (Singleton) на основе Lazy of T
    ///  Обеспечивается и потокобезопасность и "ленивость" то есть, инициализация по необходимости
    ///</summary>
    public sealed class LazySingleton
    {
        public static readonly Lazy<LazySingleton> _instance = new Lazy<LazySingleton>(() => new LazySingleton());

        LazySingleton() { }

        public static LazySingleton instance { get { return _instance.Value; } }
    }

    public sealed class OldCheckedSingleton
    {
        // Нужно volatile для обеспечения потокобезопасности (чтобы _instance не инициировалось до вызова конструктора)
        private static volatile OldCheckedSingleton? _instance;
        private static readonly object _syncObj = new object();

        OldCheckedSingleton()
        { }

        ///<summary>
        ///  Версия которая будет работать во всех версиях .NET Framework
        ///  Потокобезопасная и "ленивая"
        ///  При обращению к _instance проверяем что он еще не инициализирован, если нет то ставим блокировку и уже внутри блокировки проверяем повторно
        ///     инициализацию (она могла появиться из-за вызова параллельного потока). Если еще нет - вызываем конструктор и возвращаем, иначе - сразу возвращаем
        ///     инициированный ранее экземпляр
        ///</summary>
        public static OldCheckedSingleton Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncObj)
                    {
                        if (_instance == null)
                        {
                            _instance = new OldCheckedSingleton();
                        }
                    }
                }

                return _instance;
            }
        }
    }
}
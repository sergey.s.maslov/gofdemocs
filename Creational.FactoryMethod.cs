///<summary>
/// Фабричный метод (FactoryMethod) определяет интерфейс создания объекта, но позволяет субклассам выбрать класс 
/// создаваемого экземпляра. Таким образом, Фабричный метод делегирует операцию создания экземпляра субклассам
///</summary>
using System.Text;

namespace GOFPatterns.Creational
{
    public abstract class BasicMessage
    {
        public virtual string Format(string data)
        {
            return String.Empty;
        }
    }

    public class AlertMessage : BasicMessage
    {
        public override string Format(string data)
        {
            return $"alert! {data}";
        }
    }

    public class ErrorMessage : BasicMessage
    {
        public override string Format(string data)
        {
            return $"error! {data}";
        }
    }

    public static class LogEntryParser
    {
        public static BasicMessage Parse(string data)
        {
            if (data.IndexOf("error") > 0)
            {
                return new ErrorMessage();
            }
            else
            {
                return new AlertMessage();
            }
        }
    }
}
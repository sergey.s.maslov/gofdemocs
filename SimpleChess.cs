///<summary>
/// Общий код, используемый в нескольких примерах
/// Реализация игры в шахматы (очень странные) - из фигур есть только пешки и короли
/// Описаны классы фигур (интерфейс IChessShape) и реализующие его классы Pawn (пешка) и King (король)
/// а также класс доски который имеет метод для добавления фигур на поле
///</summary>
using System.Text;
namespace SimpleChess
{
    ///
    public interface IChessShape
    {
        void Draw();
    }

    public enum ChessShape
    {
        Pawn,
        King
    }

    public enum ChessColor
    {
        White,
        Black
    }


    public class Pawn : IChessShape
    {
        public void Draw()
        {
            Console.WriteLine("Basic Pawn");
        }
    }

    public class King : IChessShape
    {
        public void Draw()
        {
            Console.WriteLine("Basic King");
        }
    }

    public class BasicChessBoard
    {
        public string? Name;
        public List<IChessShape> Figures = new List<IChessShape>();

        public BasicChessBoard()
        {

        }

        public virtual void LoadPosition()
        {

        }

        public virtual void SavePosition()
        {

        }

        public void Add(IChessShape shape)
        {
            Figures.Add(shape);
        }

        public override string ToString()
        {
            int i = 0;
            var sb = new StringBuilder();
            sb.Append($"{{\n");

            foreach (var f in Figures)
            {
                i++;
                sb.Append($"  {f.GetType()}:{i};\n");
            }
            sb.Append($"}}\n");
            return sb.ToString();
        }
    }

}
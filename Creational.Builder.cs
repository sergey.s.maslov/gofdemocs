///<summary>
/// Строитель (Builder) позволяет отделить (возможно, сложную) операцию конструирования объекта от его представления
/// При этом в результате одного и того же процесса конструирования могут получаться разные представления 
///</summary>
///<demo>
/// Демонстрационный пример показывает применение Строителя для создания шахматной доски заполненой фигурами
/// Пример очень упрощен, реальный строитель мог бы принимать описание позиции в каком-то формате, разбирать его
/// и расставлять фигуры в нужном расположении
///</demo>
///<usage>
///  Строитель применяется в случаях, когда процесс создания является сложным и состоит из несколькх этапов, при этом
///   разные клиенты хотят получать разные состояния создаваемых объектов
///</usage>
///<dotnet>
///  StringBuilder, UriBuilder, ImmutableList<T>.Builder...
///</dotnet>

using System.Text;
using SimpleChess;

namespace GOFPatterns.Creational
{
    public sealed class ChessBoartBuilder
    {
        private readonly BasicChessBoard board = new BasicChessBoard();

        public ChessBoartBuilder AddPawns(int npawns)
        {
            int i = 0;
            while (i < npawns)
            {
                i++;
                board.Add(new Pawn());
            }
            return this;
        }

        public ChessBoartBuilder AddKings(int nkings)
        {
            int i = 0;
            while (i < nkings)
            {
                i++;
                board.Add(new King());
            }

            return this;
        }

        public BasicChessBoard Build()
        {
            return board;
        }

        public ChessBoartBuilder()
        {

        }
    }



    public class BuilderDemo
    {
        public static void Run()
        {
            var board = new ChessBoartBuilder()
                .AddPawns(6)
                .AddKings(2)
                .Build();
            Console.WriteLine(board.ToString());
        }
    }
}

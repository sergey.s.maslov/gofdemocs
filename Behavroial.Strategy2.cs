///<summary>
///  Еще один вариант применения стратегий. Решение квадратных уравнений.
///  Два варианта - один в вещественных числах второй общий (при отрицательном дискриминанте или возвращаем NaN или комплексные решения)
///</summary>
///<>
///<>

using System;
using System.Numerics;


namespace GOFPatterns.Behavioral
{
    public interface IDiscriminantStrategy
    {
        double CalculateDiscriminant(double a, double b, double c);
    }

    public class OrdinaryDiscriminantStrategy : IDiscriminantStrategy
    {
        public double CalculateDiscriminant(double a, double b, double c)
        {
            return Math.Pow(b, 2) - 4 * a * c;
        }
    }

    public class RealDiscriminantStrategy : IDiscriminantStrategy
    {
        public double CalculateDiscriminant(double a, double b, double c)
        {
            var d = Math.Pow(b, 2) - 4 * a * c;
            if (d < 0)
            {
                return double.NaN;
            }
            else
            {
                return d;
            }
        }
    }

    public class QuadraticEquationSolver
    {
        private readonly IDiscriminantStrategy strategy;

        public QuadraticEquationSolver(IDiscriminantStrategy strategy)
        {
            this.strategy = strategy;
        }

        public Tuple<Complex, Complex> Solve(double a, double b, double c)
        {
            var d = Math.Sqrt(strategy.CalculateDiscriminant(a, b, c));
            if (d == double.NaN)
            {
                ///(Complex, Complex) tuple = (new Complex(double.NaN, 0), new Complex(double.NaN, 0));
                var c1 = new Complex(double.NaN, 0);
                var c2 = new Complex(double.NaN, 0);
                var tuple = new Tuple<Complex, Complex>(c1, c2);
                return tuple;
            }
            else if (d < 0)
            {
                var x1 = -b / (2 * a);
                var x2 = Math.Sqrt(-b) / (2 * a);
                return new Tuple<Complex, Complex>(new Complex(x1, x2), new Complex(x1, -x2));
            }
            else
            {
                var x1 = (-b + Math.Sqrt(d)) / 2 * a;
                var x2 = (-b - Math.Sqrt(d)) / 2 * a;
                return new Tuple<Complex, Complex>(new Complex(x1, 0), new Complex(x2, 0));
            }

        }
    }
}
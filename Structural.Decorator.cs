///<summary>
///  Декоратор (GOF Decorator) Динамически добавляет объекту новые обязанности
///</summary>
///<demo>
/// Демонстрационный пример показывает применение Декоратора для расширения функционаьности неизменяемого (sealed) класса
///</demo>
///<dotnet>
///  System.IO.BufferedStream
///</dotnet>

using System.Diagnostics;

namespace GOFPatterns.Structural
{
    public interface IEventSaver
    {
        Task SaveEvent(string SourceId, string EventEntry);
    }

    /// Базовый класс который будем декорировать
    public sealed class PgEventSaver : IEventSaver
    {
        public Task SaveEvent(string SourceId, string EventEntry)
        {
            Console.WriteLine("SaveEvent");
            return Task.FromResult<int>(0);
        }
    }

    /// Заготовка для будущих декораторов
    /// Имеет внутреннего "декорируемого" которому будет передаваться обработка
    /// и внешний интерфейс иммитирующий декорируемого - SaveEvent()
    public abstract class EventSaverDecorator : IEventSaver
    {
        protected readonly IEventSaver _decoratee;

        protected EventSaverDecorator(IEventSaver decoratee)
        {
            _decoratee = decoratee;
        }

        public abstract Task SaveEvent(string SourceId, string EventEntry);

    }

    /// Настоящий декоратор наследуемый от абстрактного
    ///  Асинхронно вызывет метод декорируемого и запоминает за сколько он отработал
    public class TraceEventSaverDecorator : EventSaverDecorator
    {
        public TraceEventSaverDecorator(IEventSaver decoratee) : base(decoratee)
        {

        }

        public override async Task SaveEvent(string SourceId, string EventEntry)
        {
            var t = Stopwatch.StartNew();
            try
            {
                await _decoratee.SaveEvent(SourceId, EventEntry);
            }
            finally
            {
                Console.WriteLine($"done: {t.ElapsedMilliseconds}");
            }
        }
    }


    public class DecoratorDemo
    {
        public static void Run()
        {
            IEventSaver sw = new TraceEventSaverDecorator(new PgEventSaver());
            /// вызов декоратора
            Task Task = sw.SaveEvent("Test1", "Test2");
        }
    }

}
///<summary>
///  Адаптор (GOF Adapter) Преобразует интерфейс одного класса в интерфейс другого класса (который ожидают клиенты)
/// Обеспечивает совместную работу классов с несовместимыми интерфейсами
///</summary>
///
namespace GOFPatterns.Structural
{
    public class AdapterDemo
    {
        public static void Run()
        {

        }
    }

}
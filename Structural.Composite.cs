///<summary>
/// Компоновщик (Composite) позволяет скомпоновать объекты в древовидные структуры для представления иерархий
/// часть-целое. Позволяет клиентам единообразно трактовать единичные и составные объекты
///</summary>
///<dotnet>
///  XmlNode и XElement
///</dotnet>
///<demo>
/// Демонстрационный пример показывает применение Компоновщика для реализации корзины покупок в которую могут помещаться 
/// Товары и Коробки (Коробка может содержать также другие Коробки и/или Товары)
///</demo>
///
namespace GOFPatterns.Structural
{
    public class CompositeDemo
    {
        public static void Run()
        {
            var ShoppingBasket = new Box();
            ShoppingBasket.Items.Add(new Product(2, 1));
            var b = new Box();
            b.Items.Add(new Product(2, 1));
            b.Items.Add(new Product(2, 1));
            ShoppingBasket.Items.Add(b);

            Console.WriteLine($"Cost = {ShoppingBasket.Cost()}");
            Console.WriteLine($"Weight = {ShoppingBasket.Weight()}");
        }
    }

    public interface IShoppingObject
    {
        public int Cost();
        public int Weight();

    }


    public class Product : IShoppingObject
    {
        private int cost;
        private int weight;

        public Product(int pCost, int pWeight)
        {
            this.cost = pCost;
            this.weight = pWeight;
        }

        public int Cost()
        {
            return cost;
        }
        public int Weight()
        {
            return weight;
        }
    }

    public class Box : IShoppingObject
    {
        private Lazy<List<IShoppingObject>> items = new Lazy<List<IShoppingObject>>();
        public List<IShoppingObject> Items => items.Value;

        public int Cost()
        {
            var cost = 0;
            foreach (IShoppingObject i in Items)
            {
                cost += i.Cost();
            }
            return cost;

        }
        public int Weight()
        {
            var weight = 0;
            foreach (IShoppingObject i in Items)
            {
                weight += i.Weight();
            }
            return weight;
        }

    }

}
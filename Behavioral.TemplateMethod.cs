///<summary>
///  Мотивация для применения паттерна Шаблонный Метод похожа на мотивацию Стратегии
///  Типичный кандидат на применение - имеется общий алгоритм, который можно разбить на шаги
///     и какие-то шаги переопределять реализуя специфические особенности безового алгоритма
///  Стратегия (Strategy) решает задачу через композицию, Шаблонный Метод (Template Method) - через наследование
///</summary>
///<>
///<>

namespace GOFPatterns.Behavioral
{
    public abstract class BoardGame
    {
        /// В данном случае, это и есть шаблонный метод. Неважно какая это будет игра - шахматы, шашки либо "Чапаев"
        /// Игра всегда состоит из начала и последовательности шагов
        /// Наследники будут подменять шаги (Start, NewTurn) но сам шаблонный метод будет неизменным
        public void RunParty()
        {
            Start();
            while (!GameOver)
                NewTurn();
            Console.WriteLine($"Игрок {Winner} победил!");
        }

        protected abstract void Start();
        protected abstract bool GameOver { get; }
        protected abstract void NewTurn();
        protected abstract int Winner { get; }

        public BoardGame(int maxTurns)
        {
            this.numberOfPlayers = 2;
            this.maxTurns = maxTurns;
        }

        protected int currentPlayer;

        protected readonly int numberOfPlayers;
        protected int maxTurns = 10;
        protected int turn = 1;
    }


    public class ChessGame : BoardGame
    {
        public ChessGame(int maxTurns) : base(maxTurns)
        {
        }

        protected override void Start()
        {
            Console.WriteLine($"Начинается шахматная партия.");
        }

        protected override bool GameOver => turn == maxTurns;

        protected override void NewTurn()
        {
            Console.WriteLine($"Ход {turn++} игрока {currentPlayer}.");
            currentPlayer = (currentPlayer + 1) % numberOfPlayers;
        }

        protected override int Winner => currentPlayer;


    }

    ///  Второй вариант. Шаблонный метод в функциональном стиле
    public static class GameTemplate
    {
        public static void Run(
            Action start,
            Action newTurn,
            Func<bool> gameOver,
            Func<int> winningPlayer)
        {
            start();
            while (!gameOver())
                newTurn();
            Console.WriteLine($"Игрок {winningPlayer()} победил.");
        }


    }

    public class FuncChessDemo
    {
        public static void Run(int numTurns)
        {
            int numberOfPlayers = 2;
            int currentPlayer = 0;
            int turn = 1;
            var maxTurns = numTurns;

            void Start()
            {
                Console.WriteLine($"Начинается шахматная партия (функциональная реализация).");
            }

            bool GameOver()
            {
                return turn == maxTurns;
            }

            void NewTurn()
            {
                Console.WriteLine($"Turn {turn++} taken by player {currentPlayer}.");
                currentPlayer = (currentPlayer + 1) % numberOfPlayers;
            }

            int WinningPlayer()
            {
                return currentPlayer;
            }

            GameTemplate.Run(Start, NewTurn, GameOver, WinningPlayer);
        }
    }


    public class TemplateMethodDemo
    {
        public static void Run()
        {
            /// Вариант 1. Реализация с наследованием
            var game = new ChessGame(6);
            game.RunParty();

            /// Вариант 2. В функциональном стиле
            /// Класс статический просто обращаемся к методу. Создавать не нужно
            FuncChessDemo.Run(3);
        }
    }

}


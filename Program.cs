﻿// See https://aka.ms/new-console-template for more information
using System.Collections.Generic;
using System.Text;
using static System.Console;

using GOFPatterns.Creational;
using GOFPatterns.Structural;
using GOFPatterns.Behavioral;

namespace GOFPatterns
{
    public class Demo
    {
        public static void Main(string[] args)
        {
            //AbstractFactoryDemo.Run();
            //BuilderDemo.Run();
            //StrategyDemo.Run();
            //TemplateMethodDemo.Run();
            //BridgeDemo.Run();
            //FlyweightDemo.Run();
            //DecoratorDemo.Run();
            CompositeDemo.Run();

        }
    }
}
